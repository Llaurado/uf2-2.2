import java.util.ArrayList;

public class Client {

    String nif;
    String nom;
    Boolean vip;
    ArrayList<Comandes>comandes;
    public Client(String nif, String nom, Boolean corrupte) {
        this.nif = nif;
        this.nom = nom;
        this.vip = corrupte;

    }

    public Client(String nif, String nom, Boolean corrupte, ArrayList<Comandes> comandes) {
        this.nif = nif;
        this.nom = nom;
        this.vip = corrupte;
        this.comandes = comandes;
    }

    public ArrayList<Comandes> getComandes() {
        return comandes;
    }

    public void setComandes(ArrayList<Comandes> comandes) {
        this.comandes = comandes;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Boolean getVip() {
        return vip;
    }

    public void setVip(Boolean vip) {
        this.vip = vip;
    }

    @Override
    public String toString() {
        return "Client{" +
                "nif='" + nif + '\'' +
                ", nom='" + nom + '\'' +
                ", corrupte=" + vip +
                '}';
    }

}