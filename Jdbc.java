import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Jdbc {
    private static Connection myconn;
    private static ArrayList<Client> clientlist = new ArrayList<>();
    private static ArrayList<Comandes> comandalsit = new ArrayList<>();

    static {
        try {
            myconn = DriverManager.getConnection("jdbc:mysql://localhost:3306/basededatos", "root", "root");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static int menu() {
        System.out.println("Operacions:\n1.Borrar CLient\n2.Update client\n3.Mostrar client\n4.Alta nou client\n5.Nova comanda\n6.Mostrar comanda\n7.Generacio resum fact\n0.Sortir");
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();

    }


    public static void borrarClient() throws SQLException {

        Scanner sc = new Scanner(System.in);
        llegirClient();

        Statement mystmn = myconn.createStatement();

        System.out.println("Escriu el dni del client");
        String client = sc.next();
        String borrar ="delete from comandes where dni_client="+client;
        String borrar2 ="delete from client where nif="+client;
        mystmn.execute(borrar);
        mystmn.execute(borrar2);
        System.out.println("Client borrat juntamens amb les seves comandes");

    }




    public static void updateclient(Client c1) throws SQLException {

        Scanner sc = new Scanner(System.in);
        Statement mystmn = myconn.createStatement();

        llegirClient();

        System.out.println("Escriu el dni del client");
        String client = sc.next();
        String update="update client set nom="+c1.getNom()+",premium="+c1.getVip()+"where nif="+client;
        mystmn.execute(update);
    }

    public static Client mostrarclient() throws SQLException {
        Client client = null;
        Scanner sc=new Scanner(System.in);
        System.out.println("text a introduir per cercar nom del client");;
        String nom=sc.next();
        Statement st=myconn.createStatement();
        String select="select * from client where nif LIKE %"+nom+"%";
        ResultSet rs=st.executeQuery(select);
        while (rs.next()){
             client=new Client(rs.getString(1),rs.getString(2),rs.getBoolean(3));
        }
        return client;
    }


    public static void altaClient(Client c1) {
        boolean pk = primaryKeyClient(c1);
       try{
        if (pk) {
            String sentenciaSQLClient = "insert into client (nif,nom,corrupte)  values (?,?,?)";
            PreparedStatement sentenciaPreparadaclient = myconn.prepareStatement(sentenciaSQLClient);
            sentenciaPreparadaclient.setString(1, c1.getNif());
            sentenciaPreparadaclient.setString(2, c1.getNom());
            sentenciaPreparadaclient.setBoolean(3, c1.getVip());
            sentenciaPreparadaclient.executeUpdate();
            System.out.println("client inserti correctament");

            System.out.println(c1);

        } else {
            System.out.println("primary key repetida");
        }
       } catch (SQLException throwables) {
           throwables.printStackTrace();
       }
    }


    public static boolean primaryKeyClient(Client c1) {
        boolean primarykey = true;
        Statement statement = null;
        try {
            statement = myconn.createStatement();
            String selectclient = "select * from client";
            ResultSet rsclient = statement.executeQuery(selectclient);

            while (rsclient.next()) {
                Client client = new Client(rsclient.getString(1), rsclient.getString(2), rsclient.getBoolean(3));
                clientlist.add(client);
            }
            for (int i = 0; i < clientlist.size(); i++) {
                if (clientlist.get(i).getNif().equals(c1.getNif())) {
                    primarykey = false;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return primarykey;
    }

    public static boolean primaryKeyComandes(Comandes c1) {
        boolean primarykey = true;
        Statement statement = null;
        try {
            statement = myconn.createStatement();
            String selectclient = "select * from comandes";
            ResultSet rscomandes = statement.executeQuery(selectclient);

            while (rscomandes.next()) {
                Comandes comandes = new Comandes(rscomandes.getInt(1), rscomandes.getDouble(2), rscomandes.getDate(3), rscomandes.getString(4));
                comandalsit.add(comandes);
            }
            for (int i = 0; i < comandalsit.size(); i++) {
                if (comandalsit.get(i).getNum_comanda()==c1.getNum_comanda()) {
                    primarykey = false;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return primarykey;

    }

    public static void altacomanda(Comandes comandes) {
        llegirClient();
        Scanner sc = new Scanner(System.in);
        System.out.println("Escriu el dni del client seleccionat");
        String dniclient = sc.next();
        comandes.setDni_client(dniclient);
        boolean pk = primaryKeyComandes(comandes);

        try {

        if (pk) {
            String sentenciaSQLComandes = "insert into comandes (num_comanda,preu_total,data,dni_client)  values (?,?,?,?)";
            PreparedStatement sentenciaPreparadacomandes =  myconn.prepareStatement(sentenciaSQLComandes);

            sentenciaPreparadacomandes.setInt(1, comandes.getNum_comanda());
            sentenciaPreparadacomandes.setDouble(2, comandes.getPreu_total());
            sentenciaPreparadacomandes.setDate(3, (java.sql.Date)comandes.getData());
            sentenciaPreparadacomandes.setString(4, comandes.getDni_client());
            sentenciaPreparadacomandes.executeUpdate();
            System.out.println("Comandes inserit correctament.");
        }else{
            System.out.println("primary key repetida");
        }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public static Comandes mostrarcom() {
        Scanner sc = new Scanner(System.in);
        Comandes comandes = null;

        Statement statement = null;
        try {
            System.out.println("Escriu el dni del client");
            String clientdni = sc.next();
            statement = myconn.createStatement();

            String selectcomandes = "select * from comandes";
            ResultSet rscomandes = statement.executeQuery(selectcomandes);
            while (rscomandes.next()) {
                if (clientdni.equals(rscomandes.getString(4))) {
                     comandes = new Comandes(rscomandes.getInt(1), rscomandes.getDouble(2), rscomandes.getDate(3), rscomandes.getString(4));
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return comandes;

    }

    public static void llegirClient() {
        Statement statement = null;
        try {
            statement = myconn.createStatement();

            String selectclient = "select * from client";
            ResultSet rsclient = statement.executeQuery(selectclient);
            int i = 0;
            clientlist.clear();
            System.out.println("Clients");
            while (rsclient.next()) {
                clientlist.add(new Client(rsclient.getString(1), rsclient.getString(2), rsclient.getBoolean(3)));
                System.out.println(i + ") DNI: " + rsclient.getString(1) + ", NOM: " + rsclient.getString(2));
                i++;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static  void  generres_fact(int mes,int year,int dni) throws SQLException {
        Calendar cal= Calendar.getInstance();
        int añoactual=cal.get(Calendar.YEAR);
        int mesactual=cal.get(Calendar.MONTH);
        Statement st= null;

        st = myconn.createStatement();

        if(mes>0&&mes<13&&year<=añoactual&&mes<=mesactual){
            String call="call procedure proced_emmagatzemat("+mes+","+year+","+dni+")";

            st.execute(call);
            myconn.setAutoCommit(false);
            System.out.println("dates correctes");
        }else{
            System.out.println("mes o any incorrectes");
        }



    }




    public static void main(String[] args) throws SQLException, ParseException {
        Scanner sc = new Scanner(System.in);
        int opcio = menu();
        while (opcio != 0) {
            switch (opcio) {
                case 1:
                    borrarClient();
                    break;
                case 2:
                    System.out.println("Actualitzar el nom i el vip d'un client");
                    System.out.println("nou nom");
                    String nom=sc.next();
                    System.out.println("Nou vip (true o fals)");
                    Boolean vip=sc.nextBoolean();
                    Client c1=new Client(null,nom,vip);
                    updateclient(c1);

                    break;
                case 3:
                    System.out.println(mostrarclient());

                    break;
                case 4:


                    String dni;

                    System.out.println("Alta Client-----\n1.dni(sense lletra)");
                    dni = sc.next();
                    System.out.println("2.nom");
                    nom = sc.next();
                    System.out.println("3.Es premium?(True or false)");
                    vip = sc.nextBoolean();

                    c1 = new Client(dni, nom, vip, null);
                    altaClient(c1);
                    break;
                case 5:

                    System.out.println("Alta Client-----\n1.num_com");
                    int num_com = sc.nextInt();

                    System.out.println("2.preu (2 decimals XX,XX)");
                    double preu = sc.nextDouble();

                    System.out.println("3.data dd/MM/yyy");
                    String data = sc.next();

                    Date a = new SimpleDateFormat("dd/MM/yyy").parse(data);
                    Comandes comandes = new Comandes(num_com, preu, a, null);

                    altacomanda(comandes);
                    break;
                case 6:
                    llegirClient();
                    System.out.println(mostrarcom());
                    break;
                case 7:
                    System.out.println("Introdueix un mes valid:  ");
                    int mes=sc.nextInt();
                    System.out.println("Introdueix un any valid:  ");
                    int any=sc.nextInt();
                    System.out.println("Introdueix un dni:  ");
                    int dni1=sc.nextInt();
                    generres_fact(mes,any,dni1);
                    break;
            }

            opcio = menu();

        }

        myconn.close();


    }
}

/*              "DELIMITER $$ " +
                "Create procedure proced_emmagatzemat(mes_func int, any_func int ) " +
                "begin" +
                "set @data_ini=CONCAT(any_func,'-',mes_func,'-',01);" +
                "set @data_fi=CONCAT(any_func,'-',mes_func,'-',31);" +
                "set @dni_res=(select dni_client from comandes where dni_client=dni and data between @data_ini and @data_fi group by dni_client having count(*)>=1 ); " +
                "set @cant=(select sum(preu_total) from comandes where dni_client=dni and data between @data_ini and @data_fi group by dni_client having count(*)>=1  LIMIT 1  ); " +
                " insert into res_fact(mes,year,dni_client,quantitat) values(mes_func,any_func,@dni_res,@cant)          ;" +
                "set @correct=\'se ha creado bien\';" +
                "select @correct;" +
                "END$$ " +
                "DELIMITER ;"*/





